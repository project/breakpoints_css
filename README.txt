-- SUMMARY --

Breakpoints_css allows including css in an theme .info file by the named breakpoints provided by the Breakpoints module.

For example, if you have a theme named mytheme, in mytheme.info declare

	breakpoints[mybreakpoint] = (min-height: 20em)
	stylesheets-for[breakpoints.theme.mytheme.mybreakpoint][] = css/mybreakpoint.css

Where breakpoints.theme.mytheme.mybreakpoint is the machine name created by the Breakpoints module when declaring the breakpoint mybreakpoint in the theme mytheme.

Breakpoints created by other themes, modules, and through the breakpoints UI may be referenced as well.
Multiple stylesheets may be associated with a single named breakpoint.

The easiest way to find the machine name for a breakpoint is by inspecting the URLs in the Breakpoints UI configuration page.

This module was inspired by Breakpoints JS ( https://www.drupal.org/project/breakpointsjs )
and based on Conditional Stylesheets ( https://www.drupal.org/project/conditional_styles ).

For a full description of the module, visit the project page:
  https://drupal.org/projects/breakpoints_css

To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/breakpoints_css


-- REQUIREMENTS --

Breakpoints ( https://drupal.org/project/breakpoints )


-- INSTALLATION --

Install as usual, see
https://drupal.org/documentation/install/modules-themes/modules-7 for further
information.

-- CONTACT --

Current maintainers:
* James Frasche (soapboxcicero) - https://drupal.org/user/2111168

This project has been sponsored by:
* MorseMedia
  Visit https://morsemedia.net
